import testAnalyze
import sys
from os import listdir
from os.path import isfile, join

imagesPath = sys.argv[1]
outputFile = sys.argv[2]

filesList = [(join(imagesPath, f), f, outputFile) for f in listdir(imagesPath) if (isfile(join(imagesPath, f)) and f.endswith('.jpg'))]

f = open(outputFile, 'a')
f.write('file name,gray values count\n')
f.close()

[testAnalyze.borderAnalyze(file) for file in filesList]