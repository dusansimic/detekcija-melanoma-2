import cv2 as cv
import util
import numpy as np
from scipy import signal
from matplotlib import pyplot as plt
import sys

# Params: line (util.Line), image (cvimg), startPoint (tuple), endPoint (tuple)
#
# Izvlaci podatke iz linije koja se pravi za odredjene tacke oko centra lezije,
# tj. izvalce se vrednosti datih piksela. To se kasnije koristi za racunanje
# brzine kojom se prekida data lezija.
def getDataFromImage(line, image, startPoint, endPoint, contour, fileName):
	x1, y1 = startPoint
	x2, y2 = endPoint
	imageHeight, imageWidth = image.shape

	data = np.zeros(x1 - x2 if x2 < x1 else x2 - x1)
	borderPoint = None
	drawnImage = cv.drawContours(np.zeros((imageHeight, imageWidth, 1), dtype=np.uint8), [contour], 0, 255)

	if x1 == x2:
		if y1 < y2: # Dot 1
			data = np.array(image[y1:y2, x1])
			for y in range(y1, imageHeight):
				if drawnImage[y, x1] == 255:
					borderPoint = y - y1
		else: # Dot 2
			data = np.array(image[y2:y1, x1])
			for y in range(0, y1):
				if drawnImage[y, x1] == 255:
					borderPoint = y
	elif x1 > x2:
		if y1 == y2: # Dot 3
			data = np.array(image[y1, x2:x1])
			for x in range(0, x1):
				if drawnImage[y1, x] == 255:
					borderPoint = x
		else: # Dot 7 and 8
			# Trci se po konturi i trazi se tacka preseka linije i konture.
			if y1 < y2: # Dot 8
				if x1 <= y2 - y1:
					data[0] = image[y1 + x1, 0]
					for x in range(1, x1):
						data[x] = image[y1 + x1 - x, x]
						if drawnImage[y1 + x1 - x, x] == 255 or drawnImage[y1 - x1 + x, x - 1] == 255 or drawnImage[y1 + x1 - x + 1, x] == 255:
							borderPoint = x
				else:
					data[0] = image[y2, y1 + x1 - y2]
					for y in range(y2 - 1, y1, -1):
						data[y2 - y] = image[y, y1 + x1 - y]
						if drawnImage[y, y1 + x1 - y] == 255 or drawnImage[y, y1 + x1 - y - 1] == 255 or drawnImage[y + 1, y1 + x1 - y] == 255:
							borderPoint = y2 - y
			else: # Data 7
				if x1 <= y1:
					data[0] = image[y1 - x1, 0]
					for x in range(1, x1):
						data[x] = image[y1 - x1 + x, x]
						if drawnImage[y1 - x1 + x, x] == 255 or drawnImage[y1 - x1 + x, x - 1] == 255 or drawnImage[y1 - x1 + x - 1, x] == 255:
							borderPoint = x
				else:
					data[0] = image[0, x1 - y1]
					for y in range(1, y1):
						data[y] = image[y, x1 - y1 + y]
						if drawnImage[y, x1 - y1 + y] == 255 or drawnImage[y, x1 - y1 + y - 1] == 255 or drawnImage[y - 1, x1 - y1 + y] == 255:
							borderPoint = y
	else:
		if y1 == y2: # Dot 4
			data = np.array(image[y1, x1:x2])
			for x in range(x1, x2):
				if drawnImage[y1, x] == 255:
					borderPoint = x - x1
		else: # Dot 5 and 6
			# Trci se po konturi i trazi se tacka preseka linije i konture.
			if y1 < y2: # Dot 5
				if x2 - x1 <= y2 - y1:
					for x in range(x1, x2):
						data[x - x1] = image[y1 - x1 + x, x]
						if drawnImage[y1 - x1 + x, x] == 255 or drawnImage[y1 - x1 + x, x + 1] == 255 or drawnImage[y1 - x1 + x + 1, x] == 255:
							borderPoint = x - x1
					data[-1] = image[y1 - x1 + x2, x2]
				else:
					for y in range(y1, y2):
						data[y - y1] = image[y, x1 - y1 + y]
						if drawnImage[y, x1 - y1 + y] == 255 or drawnImage[y, x1 - y1 + y + 1] == 255 or drawnImage[y + 1, x1 - y1 + y] == 255:
							borderPoint = y - y1
					data[-1] = image[y2, x1 - y1 + y2]
			else: # Dot 6
				if x2 - x1 <= y1:
					for x in range(x1, x2):
						data[x - x1] = image[y1 + x1 - x, x]
						if drawnImage[y1 + x1 - x, x] == 255 or drawnImage[y1 + x1 - x, x + 1] == 255 or drawnImage[y1 + x1 - x - 1, x] == 255:
							borderPoint = x - x1
					data[-1] = image[y1 + x1 - x2 + 1, x2]
				else:
					for y in range(y1, 0, -1):
						data[y1 - y] = image[y, y1 + x1 - y]
						if drawnImage[y, y1 + x1 - y] == 255 or drawnImage[y, y1 + x1 - y + 1] == 255 or drawnImage[y - 1, y1 + x1 - y] == 255:
							borderPoint = y1 - y
					data[-1] = image[0, y1 + x1]

	return (data, borderPoint)

def fixFuckedUpContour(drawnContour):
	kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (9,9))
	dilated = cv.dilate(drawnContour, kernel)
	contours, _ = cv.findContours(dilated.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
	fixedContour = contours[0]
	return fixedContour

# Params: data (linija piksela)
#
# Ovime se odredjuje "najvece" rastojanje izmedju minimuma i maksimuma date
# linije piksela. To se moze koristiti kao validan prostor u kom se moze naci
# brzina prekida lezije.
#
# NOTE: Nije bas dobro koristiti ovaj metod. Bolje je mozda
# koristiti kvadrat prvog izvoda podataka i dalje obradjivati
# dobijene rezultate.
def findDistanceBetweenMinMax(data):
	minData = 255
	minDataIndex = 0
	maxData = 0
	maxDataIndex = 0
	for i, el in enumerate(list(data)):
		if el < minData:
			minData = el
			minDataIndex = i
		elif el > maxData:
			maxData = el
			maxDataIndex = i
	distance = abs(maxDataIndex - minDataIndex)
	return distance

# Params: data (linija piksela)
#
# Odradi se linearna regresija nad linijom piksela. Ideja je bila da se koristi
# linreg da bi videli koliko opada ali ne valja to nad svim podacima. Eventualno
# bi radilo u kombinaciji sa `findDistanceBetweenMinMax()` ali i tad je pod
# znakom pitanja.
#
# NOTE: Nije bas dobro koristiti ovaj metod. Bolje je mozda
# koristiti kvadrat prvog izvoda podataka i dalje obradjivati
# dobijene rezultate.
def workWithLinearRegression(data):
	x = range(len(data))
	m = np.divide(np.mean(x) * np.mean(data) - np.mean(x*data), np.power(np.mean(x), 2) - np.mean(np.power(x, 2)))
	b = np.mean(data) - m * np.mean(x)
	return (m, b)

# Params: data (linija piksela)
#
# Ovo je pokusaj da se radi sa nelinearnom regresijom kao sto se radilo sa
# linearnom, samo sto nije bila neka velika razlika u outputu lin i nelin reg,
# stoga sam batalio ovaj metod.
#
# NOTE: Nije bas dobro koristiti ovaj metod. Bolje je mozda
# koristiti kvadrat prvog izvoda podataka i dalje obradjivati
# dobijene rezultate.
def workWithNonLinearRegression(data):
	a1, a0 = workWithLinearRegression(np.log(data))
	a = np.exp(a0)
	b = a1
	return b, a

# Params: x (bliska x koordinata), y (bliska y koordinata), finalY (data y
# koordinata)
#
# Koristi se linearna interpolacija da bi se nasla vrednost koordinate x za
# datu vrednost koordinate y i time da se dobija mesto na kome se nalazi
# odredjeni podatak.
def findXForY(x, y, finalY):
	return (finalY*x)/y

# Params: data (linija piksela)
#
# Ovo je ko tabanje brzine preko y, ali vrlo vrlo cudno. Ideja je bila Igorova,
# cinilo se kao dobro u pocetku jer nije bilo drugih ideja ali onda sam resio da
# iskoristim Stevinu ideju da kvadriram izvod funkcije.
def findSpeedOfCurveDumbWay(data):
	xss = []
	stepSize = (max(data) - min(data))/100
	for step in np.arange(min(data), max(data), stepSize):
		for i in range(1, len(data)):
			if data[i - 1] == step:
				xss.append(i - 1)
			elif data[i] == step:
				xss.append(i)
			elif (data[i - 1] < step and data[i] > step) or (data[i - 1] > step and data[i] < step):
				xss.append((step*i)/data[i])
	diffs = []
	for i in range(1, len(xss)):
		diffs.append(xss[i] - xss[i - 1])
	return diffs

# Params: data (linija piksela)
#
# Glupav nacin za trazenje brzine pada/rasta jer sam prakticno ovde
# implementirao aproskimaciju izvoda... sto je... kao sto se da videti...
# vrlo glup potez.
# Doduse radjeno je sa dombrom namerom, da ja sam zadam moj step i onda bi mogao
# malo grublje da odredim izvod.
def findSpeedOfCurve(data):
	diffs = []
	for x in range(1, len(data), 2):
		diffs.append(data[x] - data[x - 1])
	return diffs

# Params: image (cvimg), center (koodrinate centra konture)
#
# Metod za pravljenje tacaka koje se koriste za izvlacenje linija piksela.
def createLineEndPoints(image, center):
	x1, y1 = center
	shapeY, shapeX = image.shape
	x = np.zeros(8)
	y = np.zeros(8)
	borderPoint = np.zeros(8)

	# Dot 1
	x[0] = x1
	y[0] = shapeY - 1

	# Dot 2
	x[1] = x1
	y[1] = 0

	# Dot 3
	x[2] = shapeX - 1
	y[2] = y1

	# Dot 4
	x[3] = 0
	y[3] = y1

	# Dot 5
	if shapeX - 1 - x1 < shapeY - 1 - y1:
		x[4] = shapeX - 1
		y[4] = y1 - x1 + x[4]
	elif shapeX - 1 - x1 > shapeY - 1 - y1:
		y[4] = shapeY - 1
		x[4] = x1 - y1 + y[4]
	else:
		x[4] = shapeX - 1
		y[4] = shapeY - 1

	# Dot 6
	if shapeX - 1 - x1 < y1:
		x[5] = shapeX - 1
		y[5] = y1 + x1 - x[5]
	elif shapeX - 1 - x1 > y1:
		y[5] = 0
		x[5] = y1 + x1 - y[5]
	else:
		x[5] = shapeX - 1
		y[5] = 0

	# Dot 7
	if x1 < y1:
		x[6] = 0
		y[6] = y1 - x1 + x[6]
	elif x1 > y1:
		y[6] = 0
		x[6] = x1 - y1
	else:
		x[6] = 0
		y[6] = 0

	# Dot 8
	if x1 < shapeY - 1 - y1:
		x[7] = 0
		y[7] = y1 + x1
	elif x1 > shapeY - 1 - y1:
		y[7] = shapeY - 1
		x[7] = y1 + x1 - y[7]
	else:
		x[7] = 0
		y[7] = shapeY - 1

	return np.dstack((x, y))[0]

# Params: poweredGradient (kvadriran prvi izvod podataka)
def removeSpikesFromEnds(poweredGradient):
	leftZeroIndex, rightZeroIndex = None, None
	zeroTreshold = min(poweredGradient) + 0.01
	for i in range(0, len(poweredGradient)):
		if poweredGradient[i] < zeroTreshold:
			leftZeroIndex = i
			break
	for i in range(len(poweredGradient) - 1, -1, -1):
		if poweredGradient[i] < zeroTreshold:
			rightZeroIndex = i
			break
	return (leftZeroIndex, rightZeroIndex)

# Params: data (linija piksela)
#
# Naci polinomijalnu funkciju koja navise odgovara datim podacima i vratiti
# koeficijente te funkcije u nadi pronalaska brzine promene.
def fitPolynomialFunctionToData(data):
	for degree in range(1, 50):
		coefs, residuals, _, _, _ = np.polyfit(np.log(np.array(range(1, len(data)+1))), data, degree, full=True)
		print(residuals)

# Params: poweredGradient (kvadriran izvod podataka), borderPoint (tacka u data
# gde je ivica)
#
# Naci kada pocinje i kada se zavrsava prelaz tako sto ce se kretati levo i
# desno u odnosu na pocetnu tacku, borderPoint. Time se izbegava brljanje sa
# kraja i moze se preciznije odrediti de je pad.
def spreadFromBorderPoint(poweredGradient, borderPoint):
	leftPartPoweredGradient = poweredGradient[0:borderPoint]
	rightPartPoweredGradient = poweredGradient[borderPoint:len(poweredGradient)]

	leftPartMinimum, rightPartMinimum = min(leftPartPoweredGradient) + 0.01, min(rightPartPoweredGradient) + 0.01

	leftZero, rightZero = None, None
	for i in range(len(leftPartPoweredGradient) - 1, -1, -1):
		if leftPartPoweredGradient[i] < leftPartMinimum:
			leftZero = i
			break
	for i in range(0, len(rightPartPoweredGradient)):
		if rightPartPoweredGradient[i] < rightPartMinimum:
			rightZero = i + borderPoint
			break
	if leftZero == None:
		for i in range(0, len(poweredGradient)):
			if poweredGradient[i] > 0.02:
				leftZero = i - 1
				break
	if rightZero == None:
		for i in range(len(poweredGradient) - 1, -1, -1):
			if poweredGradient[i] > 0.02:
				rightZero = i + 1
				break
	return (leftZero, rightZero)

# Params: contour (kontura), image (cvimg)
#
# Mnogo spageta 🍝
def getBorderCutSpeed(contour, image, fileName):
	centerX, centerY = util.getCenterOfContour(contour)
	selectedPoints = createLineEndPoints(image, (centerX, centerY))

	imageHeight, imageWidth = image.shape
	drawnContour = cv.drawContours(np.zeros((imageHeight, imageWidth, 1), dtype=np.uint8), [contour], 0, 255)
	contour = fixFuckedUpContour(drawnContour)

	speeds = []

	for index, point in enumerate(selectedPoints):
		pointX = point[0]
		pointY = point[1]
		line = util.Line((centerX, centerY), (pointX, pointY))
		if centerX == pointX or centerY == pointY:
			line = None
		data, borderPoint = getDataFromImage(line, image, (centerX, centerY), (int(pointX), int(pointY)), contour, fileName)

		# Primena Savitzky-Golay filtera da se izravnaju dobijeni podaci.
		filteredData = signal.savgol_filter(data, int(len(data)/4) if int(len(data)/4) % 2 == 1 else int(len(data)/4)-1, 3)

		# Trazenje prvog izvoda dobijenih podataka da bi se dobili intenziteti
		# prelaza.
		gradient = np.gradient(filteredData)

		# Kvadrira se izvod da bi se resili negativnih vrednosti.
		poweredGradient = np.power(gradient, 2)

		# Odrediti maksimum izvoda.
		maxPoweredGradientIndex = np.argmax(poweredGradient)

		# Odrediti pocetak i kraj prelaza.
		leftZero, rightZero = spreadFromBorderPoint(poweredGradient, borderPoint)

		speeds.append(np.divide(rightZero - leftZero, len(poweredGradient)))

	return speeds
