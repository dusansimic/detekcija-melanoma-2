import cv2 as cv
import numpy as np
import util

image = np.zeros([100,100,1],dtype=np.uint8)

x1,y1 = (14, 23)
x2,y2 = (75, 53)

image[x1,y1] = 255
image[x2,y2] = 255

myLine = util.Line((x2, y2), (x1, y1))

for x in range(x1+1, x2):
	y = round(myLine.getYForX(x))
	image[x,y] = 255

cv.imshow('Image', image)
cv.waitKey(0)
cv.destroyAllWindows()
