import util
import sys
import cv2 as cv

image = util.readImage(sys.argv[1])
hsvimg = util.getImageInHSV(image)

thresh, contours = util.getThreshAndContour(image)

# imageCenterY, imageCenterX, _ = image.shape

# yThreshold = imageCenterY/4
# xThreshold = imageCenterX/4
# imageCenterY /= 2
# imageCenterX /= 2

# mx = 0
# mi = 0
# for i in range(len(contours)):
# 	xCenter, yCenter = util.getCenterOfContour(contours[i])
# 	if xCenter == -1 and yCenter == -1:
# 		continue
# 	if mx < cv.contourArea(contours[i]) and xCenter > imageCenterX - xThreshold and xCenter < imageCenterX + xThreshold and yCenter > imageCenterY - yThreshold and yCenter < imageCenterY + yThreshold:
# 		mx = cv.contourArea(contours[i])
# 		mi = i

# my = 0
# myi = 0
# for i in range(len(contours)):
# 	if my < cv.contourArea(contours[i]) and i != mi:
# 		my = cv.contourArea(contours[i])
# 		myi = i

# print(mx, mi)

contour = util.findLargestContour(image, contours)

imgMask = util.getMaskFromContour(image, contour)

cv.imshow('Image', image)
cv.waitKey(0)
cv.destroyAllWindows()

cv.imshow('Mask', imgMask)
cv.waitKey(0)
cv.destroyAllWindows()