import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import math

def findEdgesLaplacian(image):
	laplacian = cv.Laplacian(image, cv.CV_64F, 3)
	laplacian_abs = cv.convertScaleAbs(laplacian)
	return laplacian_abs
	# return (laplacian_abs, sobelX_abs, sobelY_abs)

def findEdgesSobelX(image):
	sobelX = cv.Sobel(image, cv.CV_64F, 1, 0, ksize=3, scale=1, delta=0, borderType=cv.BORDER_DEFAULT)
	sobelX_abs = cv.convertScaleAbs(sobelX)
	return sobelX_abs

def findEdgesSobelY(image):
	sobelY = cv.Sobel(image, cv.CV_64F, 0, 1, ksize=3, scale=1, delta=0, borderType=cv.BORDER_DEFAULT)
	sobelY_abs = cv.convertScaleAbs(sobelY)
	return sobelY_abs

def findEdgesCanny(image):
	canny = cv.Canny(image, 100, 100)
	return canny

def houghTransform(grayImage, sobelX):
	lines = cv.HoughLines(grayImage, 1, np.pi / 180, 150, None, 0, 0)
	if lines is not None:
		for i in range(len(lines)):
			rho = lines[i][0][0]
			theta = lines[i][0][1]
			a = math.cos(theta)
			b = math.sin(theta)
			x0 = a * rho
			y0 = b * rho
			pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
			pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))

			cv.line(grayImage, pt1, pt2, (0,0,255), 3, cv.LINE_AA)

	return grayImage

def getDermoscopicStructures(grayImage):
	laplacian = findEdgesLaplacian(grayImage)
	sobelX = findEdgesSobelX(grayImage)
	sobelY = findEdgesSobelY(grayImage)
	canny = findEdgesCanny(grayImage)
	houghTransformImage = houghTransform(grayImage, sobelX)
	return (laplacian, sobelX, sobelY, canny, houghTransformImage)
