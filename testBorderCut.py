import sys
import cv2 as cv
import border
import util

image = cv.imread(sys.argv[1])
thresh, contours = util.getThreshAndContour(image)
contour = util.findLargestContour(contours)
imgMask = util.getMaskFromThresh(image, contour)
grayImage = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

border.getBorderCutSpeed(contour, grayImage)
