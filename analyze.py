#!/usr/bin/env python3
from multiprocessing import Pool
from main import analyze
from os import listdir
from os.path import isfile, join
import sys
import tqdm
import datetime
import platform
import cv2
from colorama import Fore

print(Fore.GREEN + 'OS: {}\nPython: {}\nOpenCV: {}'.format(platform.platform(), platform.python_version(), cv2.__version__) + Fore.RESET)

imagesPath = sys.argv[1]
noProcesses = int(sys.argv[2])
if len(sys.argv) > 3:
	outputFile = '{}.{}.csv'.format(sys.argv[3], datetime.datetime.now().isoformat())
	f = open(outputFile, 'a')
	f.write('file name,hue standard deviation,saturation standard deviation,value standard deviation,speed up,speed down,speed left,speed right,speed up right,speed down right,speed down left,speed up left\n')
	f.close()

outputFile = 'none'
filesList = [(join(imagesPath, f), f, outputFile) for f in listdir(imagesPath) if (isfile(join(imagesPath, f)) and f.endswith('.jpg'))]

with Pool(noProcesses) as p:
	for _ in tqdm.tqdm(p.imap(analyze, filesList), total=len(filesList)):
		pass