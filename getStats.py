import sys
import numpy as np
import math
import matplotlib.pyplot as plt

hueStandardDeviation = []
saturationStandardDeviation = []
valueStandardDeviation = []

with open(sys.argv[1], 'r') as csvfile:
	for line in csvfile.readlines():
		splitLine = line.replace('\n', '')
		splitLine = splitLine.split(',')
		if splitLine[1] == 'hue standard deviation':
			continue
		hueStandardDeviation.append(float(splitLine[1]))
		saturationStandardDeviation.append(float(splitLine[2]))
		valueStandardDeviation.append(float(splitLine[3]))

hueStandardDeviationGroups = np.zeros(8)
saturationStandardDeviationGroups = np.zeros(8)
valueStandardDeviationGroups = np.zeros(8)

for hueEl in hueStandardDeviation:
	for i in range(7):
		if hueEl >= 16.0*i and hueEl < 16.0*(i+1):
			hueStandardDeviationGroups[i] += 1

for saturationEl in saturationStandardDeviation:
	for i in range(7):
		if saturationEl >= 16.0*i and saturationEl < 16.0*(i+1):
			saturationStandardDeviationGroups[i] += 1

for valueEl in valueStandardDeviation:
	for i in range(7):
		if valueEl >= 16.0*i and valueEl < 16.0*(i+1):
			valueStandardDeviationGroups[i] += 1

labels = [str(str(16*i) + '\n' + str(16*(i+1))) for i in range(8)]

x = np.arange(len(labels))
width = 0.2

fig, ax = plt.subplots()
rects1 = ax.bar(x - width, hueStandardDeviationGroups, width, label='Hue')
rects2 = ax.bar(x, saturationStandardDeviationGroups, width, label='Saturation')
rects3 = ax.bar(x + width, valueStandardDeviationGroups, width, label='Value')

ax.set_ylabel('Amount')
ax.set_title('Color deviations')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

def autolabel(rects):
	"""Attach a text label above each bar in *rects*, displaying its height."""
	for rect in rects:
		height = rect.get_height()
		ax.annotate('{}'.format(int(height)),
								xy=(rect.get_x() + rect.get_width() / 2, height),
								xytext=(0, 3),  # 3 points vertical offset
								textcoords="offset points",
								ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

fig.tight_layout()

plt.show()
