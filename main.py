import cv2 as cv
import color
import border
import dermoscopicStructures as dermStruct
import util
import sys

def analyze(params):
	# extract image path, file name, and results file name
	imagePath, fileName, outputFilePath = params
	# read image from file
	img = util.readImage(imagePath)
	# convert image to hsv color from bgr
	hsvimg = util.getImageInHSV(img)
	# get threshold and contours for image
	thresh, contours = util.getThreshAndContour(img)
	# find largest contour (the lesion contour)
	contour = util.findLargestContour(img, contours)
	# create an image mask based of that contour
	imgMask = util.getMaskFromContour(img, contour)
	# make a grayscale image from bgr
	grayImage = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

	# apply mask to the hsv image
	hsvImgMasked = util.applyMask(hsvimg, imgMask)

	# Get Border Rule
	# Border cut speed is the amount of fade between lesion color and skin color.
	# This will help us see if the lesion has an abrupt cut or it fades slowly
	# into the skin color.
	speeds = border.getBorderCutSpeed(contour, grayImage, fileName)

	# Get Color Rule
	# This will get the deviation of colors on the whole lesion.
	stddevs, means = color.colorDeviation(hsvImgMasked)

	# # Get dermoscopic structures
	# laplacian, sobelX, sobelY, canny, houghTransformImage = dermStruct.getDermoscopicStructures(grayImage)
	# cv.imshow('Original', grayImage)
	# cv.waitKey(0)
	# cv.destroyAllWindows()
	# cv.imshow('Hough Transform Image', houghTransformImage)
	# cv.waitKey(0)
	# cv.destroyAllWindows()

	# csvLine = '{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(str(fileName), str(stddevs[0]), str(stddevs[1]), str(stddevs[2]), str(speeds[0]), str(speeds[1]), str(speeds[2]), str(speeds[3]), str(speeds[4]), str(speeds[5]), str(speeds[6]), str(speeds[7]))
	# csvLine = '{},{},{},{}\n'.format(str(fileName), str(stddevs[0]), str(stddevs[1]), str(stddevs[2]))
	# file = open(outputFilePath, 'a')
	# file.write(csvLine)
	# file.close()
