import cv2 as cv
import numpy as np

# Traženje kontura gledajući samo sredinu slike
def findLargestContour(image, contours):
	imageCenterY, imageCenterX, _ = image.shape

	yThreshold = imageCenterY/4
	xThreshold = imageCenterX/4
	imageCenterY /= 2
	imageCenterX /= 2

	maxa = 0
	maxi = 0
	for i in range(len(contours)):
		xCenter, yCenter = getCenterOfContour(contours[i])
		if xCenter == None and yCenter == None:
			continue
		if maxa < cv.contourArea(contours[i]) and xCenter > imageCenterX - xThreshold and xCenter < imageCenterX + xThreshold and yCenter > imageCenterY - yThreshold and yCenter < imageCenterY + yThreshold:
			maxa = cv.contourArea(contours[i])
			maxi = i

	return contours[maxi]

def getCenterOfContour(contour):
	M = cv.moments(contour)
	if M['m00'] == 0:
		return (None, None)
	x = int(M['m10'] / M['m00'])
	y = int(M['m01'] / M['m00'])
	return (x, y)

def __increaseContrast(img, brightness, contrast):
	imgInceasedContrast = cv.addWeighted(img, 1.0 + contrast / 127, img, 0, brightness - contrast)
	return imgInceasedContrast

def getThreshAndContour(image):
	imgIncreasedContrast = __increaseContrast(image, 0.0, 64.0)
	imggray = cv.cvtColor(imgIncreasedContrast, cv.COLOR_BGR2GRAY) # Convert to BW
	blur = cv.GaussianBlur(imggray, (5,5), 0)
	_, thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY_INV | cv.THRESH_OTSU) # Getting threshold
	contours, _ = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE) # Getting contour

	return (thresh, contours)

def getMaskFromContour(img, contour):
	# Get image shape (height and width)
	maskShapeHeight, maskShapeWidth, _ = img.shape
	# Paint it black
	maskImg = np.zeros((maskShapeHeight, maskShapeWidth, 1), dtype=np.uint8)
	# Draw contour in white
	maskImg = cv.drawContours(maskImg, [contour], 0, 255)
	# Fill contour in white
	([contour], maskImg) = fillEmpty([contour], maskImg)
	
	return maskImg

def removeHoles(thresh):
	kernel = np.ones((1,1), np.uint8)
	closing = cv.morphologyEx(thresh, cv.MORPH_CLOSE, kernel)
	return closing

def fillEmpty(contours, thresh):
	for cnt in contours:
		cv.drawContours(thresh, [cnt], 0, 255, -1)
	return (contours, thresh)

def removeNoise(thresh):
	kernel = np.ones((1,1), np.uint8)
	opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel)
	return opening

def readImage(imagePath): return cv.imread(imagePath)

def getImageInHSV(image): return cv.cvtColor(image, cv.COLOR_BGR2HSV)

def applyMask(image, mask): return cv.bitwise_and(image, image, mask = mask)

def doKMeans(image, k):
	Z = image.reshape((-1, 3))
	Z = np.float32(Z)

	criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
	ret, label, center = cv.kmeans(Z, k, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)

	center = np.uint8(center)
	res = center[label.flatten()]
	res2 = res.reshape((image.shape))

	return res2

class Line:
	def __init__(self, firstPoint, secondPoint):
		self.x1, self.y1 = firstPoint
		self.x2, self.y2 = secondPoint
		x = self.x2 if self.x2 == self.x1 else self.x2 - self.x1
		y = self.y2 if self.y2 == self.y1 else self.y2 - self.y1
		self.k = y/x

	def getYForX(self, x):
		return self.k * x - self.k * self.x1 + self.y1

	def getCoefficients(self):
		return (self.k, -self.k*self.x1+self.y1)
