import cv2 as cv
import util
import border
from matplotlib import pyplot as plt
import numpy as np

def borderAnalyze(args):
	imPath, imName, outputFile = args
	img = util.readImage(imPath)
	thresh, contours = util.getThreshAndContour(img)
	contour = util.findLargestContour(contours)
	imgMask = util.getMaskFromThresh(img, contour)

	hsvImgMasked = util.applyMask(img, imgMask)

	# cv.imshow('Masked', hsvImgMasked)
	# cv.waitKey(0)
	# cv.destroyAllWindows()

	# for coord in contour:
	imageHeight, imageWidth, _ = img.shape
	for coord in contour:
		if coord[0][0] < 50 or coord[0][1] < 50 or (imageHeight - coord[0][1]) < 50 or (imageWidth - coord[0][0]) < 50:
			continue
		frame = border.getFrame(img, coord[0], (100, 100))
		grayFrame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

		frameRGB = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

		# plt.subplot(2,2,1),plt.imshow(frameRGB)
		# plt.title('Original'), plt.xticks([]), plt.yticks([])
		# plt.subplot(2,2,2),plt.imshow(grayFrame, cmap = 'gray')
		# plt.title('Grayscale'), plt.xticks([]), plt.yticks([])
		# plt.subplot(2,2,3),plt.hist(grayFrame.flatten(), facecolor='green', alpha=0.75)
		# plt.title('Histogram'), plt.xticks([]), plt.yticks([])

		grayValuesCount = np.zeros(256).astype(int)
		for pix in grayFrame.flatten():
			grayValuesCount[pix] += 1

		csvline = '{},{}\n'.format(imName, ' '.join(grayValuesCount.astype(str)))
		f = open(outputFile, 'a')
		f.write(csvline)
		f.close()

		# plt.show()
		break
