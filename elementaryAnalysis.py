#!/usr/bin/python3
import cv2 as cv
import util
import sys

image = util.readImage(sys.argv[1])
hsvimg = util.getImageInHSV(image)
tresh, contours = util.getThreshAndContour(image)
contour = util.findLargestContour(contours)
imgMask = util.getMaskFromThresh(image, contour)
grayImage = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

imageMasked = util.applyMask(image, imgMask)

cv.imshow('Image masked', imageMasked)
cv.waitKey(0)
cv.destroyAllWindows()
