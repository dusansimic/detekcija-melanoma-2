import cv2 as cv
import numpy as np

def colorDeviation(img):
	# Leave out black pixels (those which are removed by mask)
	# TODO: Should remove those outside the mask, not all black ones
	tempImgArr = img[
		(img[:,:,0] != 0) |
		(img[:,:,1] != 0) |
		(img[:,:,2] != 0)]

	# Calculate standard deviations and means (just for later analysis)
	stddevs = np.std(tempImgArr, axis=0)
	means = np.mean(tempImgArr, axis=0)

	return (stddevs, means)
