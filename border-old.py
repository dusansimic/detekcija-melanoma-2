import cv2 as cv
import util
import numpy as np
from matplotlib import pyplot as plt
import sys

def getFrame(image, center, frameSize):
	centerX = center[0]
	centerY = center[1]
	frameWidth, frameHeight = frameSize

	frame = image[(centerY - int(frameHeight/2)):(centerY + int(frameHeight/2)), (centerX - int(frameWidth/2)):(centerX + int(frameWidth/2))]

	return frame

def __getDataFromLine(line, image, startPoint, endPoint):
	shapeY, shapeX = image.shape
	x1, y1 = startPoint
	x2, y2 = endPoint
	data = None
	# Napravit array duzine koliko ima piksela izmedju dve x koordinate
	data = np.array(x1 - x2 if x1 > x2 else x2 - x1)
	# Ako su x koordinate jednake, tj. ako je vertikalna linija
	if x1 == x2:
		if y1 < y2:
			# Dot 1
			data = np.array(image[y1:y2, x1:x1+1])
		else:
			# Dot 2
			data = np.array(image[y2:y1, x1:x1+1])
	elif x1 > x2:
		if y1 == y2:
			# Dot 3
			data = np.array(image[y1:y1+1, x2:x1])
		else:
			# Dot 7 and 8
			for x in range(x2, x1):
				# print(x, int(round(line.getYForX(x))))
				data[x-x2] = image[int(round(line.getYForX(x))), x]
	else:
		if y1 == y2:
			# Dot 4
			data = np.array(image[y1:y1+1, x1:x2])
		else:
			# Dot 5 and 6
			for x in range(x1, x2):
				data[x-x1] = image[int(round(line.getYForX(x))), x]

	return data
	# return np.array([image[int(round(line.getYForX(x))), x] for x in range(startX, endX)])

def findDistanceBetweenMinMax(data):
	minData = 255
	minDataIndex = 0
	maxData = 0
	maxDataIndex = 0
	for i, el in enumerate(data):
		if el < minData:
			minData = el
			minDataIndex = i
		elif el > maxData:
			maxData = el
			maxDataIndex = i
	distance = abs(maxDataIndex - minDataIndex)
	return distance

def getHigherAndLower(firstPoint, secondPoint):
	x1, y1 = firstPoint
	x2, y2 = secondPoint

	higherX, higherY, lowerX, lowerY = 0,0,0,0

	if x1 > x2:
		higherX = x1
		lowerX = x2
	elif x2 > x1:
		higherX = x2
		lowerX = x1
	else:
		higherX = x1 + 1
		lowerX = x1

	if y1 > y2:
		higherY = y1
		lowerY = y2
	elif y2 > y1:
		higherY = y2
		lowerY = y1
	else:
		higherY = y1 + 1
		lowerY = y1

	return (int(higherX), int(higherY), int(lowerX), int(lowerY))

def getBorderCutSpeed(contour, image):
	centerX, centerY = util.getCenterOfContour(contour)
	shapeY, shapeX = image.shape
	x = np.zeros(8)
	y = np.zeros(8)
	# Tacka 1 (Gore)
	x[0] = centerX
	y[0] = shapeY - 1
	# Tacka 2 (Dole)
	x[1] = centerX
	y[1] = 0
	# Tacka 3 (Levo)
	x[2] = 0
	y[2] = centerY
	# Tacka 4 (Desno)
	x[3] = shapeX - 1
	y[3] = centerY
	if shapeY > shapeX:
		# Tacka 5 (Gore Desno)
		x[4] = shapeX - 1
		y[4] = centerY + centerX
		# Tacka 6 (Dole Desno)
		x[5] = shapeX - 1
		y[5] = centerY - centerX
		# Tacka 7 (Dole Levo)
		x[6] = 0
		y[6] = centerY - centerX
		# Tacka 8 (Gore Levo)
		x[7] = 0
		y[7] = centerY + centerX
	elif shapeY < shapeX:
		# Tacka 5 (Gore Desno)
		y[4] = shapeY - 1
		x[4] = centerX + centerY
		# Tacka 6 (Dole Desno)
		y[5] = 0
		x[5] = centerX + centerY
		# Tacka 7 (Dole Levo)
		y[6] = 0
		x[6] = centerX - centerY
		# Tacka 8 (Gore Levo)
		y[7] = shapeY - 1
		x[7] = centerX - centerY
	else:
		# Tacka 5
		x[4] = shapeX - 1
		y[4] = shapeY - 1
		# Tacka 6
		x[5] = shapeX - 1
		y[5] = 0
		# Tacka 7
		x[6] = 0
		y[6] = 0
		# Tacka 8
		x[7] = 0
		y[7] = shapeY - 1

	distances = np.zeros(8)

	for i in range(8):
		line = None
		if centerX != x[i]:
			line = util.Line((centerX, centerY), (int(x[i]), int(y[i])))

		data = __getDataFromLine(line, image, (centerX, centerY), (int(x[i]), int(y[i])))
		data = data.flatten()

		distances[i] = findDistanceBetweenMinMax(data)

	return np.divide(distances, len(data))
